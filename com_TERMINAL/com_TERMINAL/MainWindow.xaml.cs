﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Ports;
using System.Threading;

namespace com_TERMINAL
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private delegate void setTextDeleg(string text);
        bool Error = false;
        int butSettingClick = 1;        //счетчик нажатий на кнопку установки соединения
        private bool ConnectFlag = false;       //флаг указывающий на установление соединения
        private SerialPort _serialPort = new SerialPort();         //экземпляр класса SerialPort
        private List<string> GetPortName=new List<string>();           //массив для хранения имен все вкл. портов 
        List<int> SpeedBoaud = new List<int>()
        {   300,
            600,
            1200,
            2400,
            9600,
            19200,
            38400,
            57600,
            115200,
            230400,
            460800,
            921600
        };          //список скоростей терминала

        private List<string> PARITYbITS = new List<string>()
        {
            "None",
            "1"
        };          //список для битов четности

        private List<int> dataFormats = new List<int>()
        {
            7,
            8,
            9
        };      //список форматов данных

        private List<int> StopBits = new List<int>()
        {
           1,
           2
           
        };      //список стоп битов

        public MainWindow()
        {
            InitializeComponent();
            But_sendData.IsEnabled = false;
            Setting_Init();



        }

        #region Настройка соединения

        /// <summary>
        /// Метод инициализации настройки соединения, подгрузка всех выпадающих списков
        /// </summary>
        void Setting_Init()
        {
            GetPortName.AddRange(SerialPort.GetPortNames());        //получаем список имен портов
            Name_COMPort.ItemsSource = GetPortName;       //загружаем список имен портов
            Baud_ComboBox.ItemsSource = SpeedBoaud;       //загружаем список скоростей порта
            Combo_Parity.ItemsSource = PARITYbITS;        //загружаем список битов четности
            Combo_DataBits.ItemsSource = dataFormats;                 //загружаем список формата данных
            Combo_StopBits.ItemsSource = StopBits;          //загружаем список стоп битов


        }

        #endregion


        private string NamePort
        {
            get
            {
                return (string)Name_COMPort.SelectedItem;
            }
        }
        private int GetBaud
            {
            get
            {
                return (int)Baud_ComboBox.SelectedItem;
            }
            }


        private string GetParityBit
        {
            get
            {
                return (string)Combo_Parity.SelectedItem;
            }
        }


        private int GetFormatData
        {
            get
            {
                return (int)Combo_DataBits.SelectedItem;
            }
        }


        private int GetStopBits
        {
            get
            {
                return (int)Combo_StopBits.SelectedItem;
            }
        }


        /// <summary>
        /// Обработчик нажатия кнопки передачи данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void But_sendData_Click(object sender, RoutedEventArgs e)
        {
            if(ConnectFlag)
            {

                //------------------------------------------------------------------------
                //вызываем функцию отправки данных через COM  порт
                //------------------------------------------------------------------------
                Transmit_Data(Input_Data.Text);
                //------------------------------------------------------------------------
                Input_Data.Clear();
            }
            else
            {
                MessageBox.Show("Ошибка системы!!!");
            }
        }


        /// <summary>
        /// Обработчик нажатия на кнопку настройки связи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void but_check_Settings_Click(object sender, RoutedEventArgs e)
        {

            
            if (!Error)
            {
                butSettingClick++;
               
            }
            
            if(butSettingClick%2==0 && Error==false)
            {
                Main_terminal.Clear();
                Main_terminal_HEX.Clear();
                Set_Connection(_serialPort);
                but_check_Settings.Content = "Disconnection";
                ConnectFlag = true;
                But_sendData.IsEnabled = true;
                _serialPort.DataReceived += _serialPort_DataReceived;
            }
            else
            {
               
                but_check_Settings.Content = "Connection";
                MessageBox.Show("Разъединение связи с COM-портом!!!");
                ConnectFlag = false;
                But_sendData.IsEnabled = false;
                
                //вызываем функцию закрытия канала 

                ClosePort();

            }
           
            

        }

       


        #region Настройка канала
        /// <summary>
        /// Метод настройки соединения
        /// </summary>
        /// <param name="_sp"></param>
        void Set_Connection(SerialPort _sp)
        {
            
            if(Name_COMPort.SelectedIndex!=-1 && Baud_ComboBox.SelectedIndex!=-1
                && Combo_Parity.SelectedIndex!=-1 && Combo_DataBits.SelectedIndex!=-1 && Combo_StopBits.SelectedIndex!=-1)
            {
                _sp.PortName = NamePort;
                _sp.BaudRate = GetBaud;
                if(GetParityBit == "1")
                {
                    _sp.Parity = Parity.Mark;
                }
                else if(GetParityBit=="None")
                {
                    _sp.Parity = Parity.None;
                }
                _sp.DataBits = GetFormatData;
                
                if(GetStopBits==1)
                {
                    _sp.StopBits = System.IO.Ports.StopBits.One;
                }
                else if(GetStopBits==2)
                {
                    _sp.StopBits = System.IO.Ports.StopBits.Two;
                }

                try
                {
                    _sp.Open();
                    Error = false;
                }
                catch(UnauthorizedAccessException)
                {
                    Error = true;
                }
                catch(ArgumentException)
                {
                    Error = true;
                }
                if(!Error)
                {
                    MessageBox.Show("Соединение установлено. Ожидание данных.........");
                }
                else
                {
                    MessageBox.Show("Ошибка!!!Проверьте правильность выбора!!!");
                    Error = false;
                }



            }
            else
            {
                MessageBox.Show("Ошиибок!!! не выбран один из элементов настройки соединения!!!");
            }
            

        }
        #endregion



        #region Прием и обработка данных

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                    (ThreadStart)delegate ()
                    {
                        Time_event.Text = String.Format("Время приема данных-[{0}]", DateTime.Now);
                    });
                
                    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                    (ThreadStart)delegate ()
                    {
                        Status_bar_text.Text = String.Format("Прием данных......");
                    });
                Thread.Sleep(50);
                string Data = _serialPort.ReadLine();
                Dispatcher.BeginInvoke(new setTextDeleg(Receive_dat), new object[] { Data });
               
            }
            catch(Exception exs)
            {
                MessageBox.Show("Ошибка!!" + exs.Message);
            }
            

        }



        private void Receive_dat(string Data)
        {
            Main_terminal.Text += string.Format("Время приема данных[{0}]-{1}\r\n", DateTime.Now, Data.Trim());
            ConvertBytes(Data);
        }

        #endregion


        #region Передача данных через COM порт

        private void Transmit_Data(string Data)
        {
            try
            {
                _serialPort.WriteLine(String.Format("{0}", Data));
                
               

                string buffer = String.Format("[{0}]:{1}",DateTime.Now, Data);
                Main_terminal.Text+= buffer + Environment.NewLine;
                ConvertBytes(Data);
                Status_bar_text.Text = "Передача данных.....";
                Time_event.Text = String.Format("Время передачи данных-[{0}]", DateTime.Now);

            }
            catch(Exception eexs)
            {
                MessageBox.Show("Ошибка!!!" + eexs.Message);
            }
        }

        #endregion



        void ClosePort()
        {
            if(_serialPort!=null)
            {
                _serialPort.DataReceived -= _serialPort_DataReceived;
                _serialPort.Close();
               
            }
        }




        void ConvertBytes(string DataConvert)
        {
            byte[] byffer = new byte[256];
            byffer = Encoding.ASCII.GetBytes(DataConvert);
            string buffForArea;
            Main_terminal_HEX.Text += String.Format("Передача данных[{0}]:", DateTime.Now)+"\r\n";
            for (int i=0;i< byffer.Length;i++)
            {
                byte b = byffer[i];
                buffForArea = b.ToString("X4");
               
               Main_terminal_HEX.Text += buffForArea + Environment.NewLine;
               
            }
            Main_terminal_HEX.Text += "------------------\r\n";
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ClosePort();
            
        }

        private void Main_terminal_TextChanged(object sender, TextChangedEventArgs e)
        {
           Main_terminal.ScrollToEnd();
           Main_terminal_HEX.ScrollToEnd();
        }

        private void Main_terminal_HEX_TextChanged(object sender, TextChangedEventArgs e)
        {
          
        }

        private void Input_Data_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key==Key.Enter)
            {
                if (ConnectFlag)
                {

                    //------------------------------------------------------------------------
                    //вызываем функцию отправки данных через COM  порт
                    //------------------------------------------------------------------------
                    Transmit_Data(Input_Data.Text);
                    //------------------------------------------------------------------------
                    Input_Data.Clear();
                }
                else
                {
                    MessageBox.Show("Ошибка системы!!!");
                }
            }
        }
    }
}
